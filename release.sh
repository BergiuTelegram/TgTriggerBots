version=$(cat VERSION)

docker login registry.gitlab.com

docker build -t tgtriggerbots .

docker tag tgtriggerbots registry.gitlab.com/bergiutelegram/tgtriggerbots:latest
docker tag tgtriggerbots "registry.gitlab.com/bergiutelegram/tgtriggerbots:$version"

docker push registry.gitlab.com/bergiutelegram/tgtriggerbots:latest
docker push "registry.gitlab.com/bergiutelegram/tgtriggerbots:$version"
