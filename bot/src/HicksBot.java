import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.Map;

import org.telegram.telegrambots.api.methods.send.SendMessage;
import org.telegram.telegrambots.api.objects.Message;
import org.telegram.telegrambots.api.objects.Update;
import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.exceptions.TelegramApiException;
import org.telegram.telegrambots.logging.BotLogger;

public class HicksBot extends TelegramLongPollingBot {

	private String botUsername;
	private String botToken;
	private String git;
	private String dsgvo;

	public HicksBot(String username, String token, String git, String dsgvo){
		this.botUsername = username;
		this.botToken = token;
		this.git = git;
		this.dsgvo = dsgvo;
	}

	@Override
	public String getBotUsername() {
		return this.botUsername;
	}

	@Override
	public String getBotToken() {
		return this.botToken;
	}

	@Override
	public void onUpdateReceived(Update update) {
		if (update.hasMessage() && update.getMessage().hasText()) {
			String text = update.getMessage().getText().toLowerCase();
			SendMessage message = new SendMessage();
			message.setChatId(update.getMessage().getChatId());
			//write everything in lowercase in the contains
			if(text.equals("/license") || text.equals("/license"+this.getBotUsername())){
				String msg = "Welcome!\nThis bot is a program which is available under the MIT license at " + this.git;
				message.setText(msg);
			} else if(text.equals("/dsgvo") || text.equals("/dsgvo"+this.getBotUsername())) {
				String msg = this.dsgvo;
				message.setText(msg);
			}
			else if(text.contains("hicks")){
				message.setText("*Gesundheit!*");
			} else if (text.contains("bier") || text.contains("alkohol")){
				message.setText("_HICKS_");
			} else if (text.contains("goofy")){
				message.setText("*AHIAK*");
			} else {
				return;
			}
			message.setParseMode("markdown");
			try {
				sendMessage(message); // Call method to send the message
			} catch (TelegramApiException e) {
				e.printStackTrace();
			}
		}
	}

}
