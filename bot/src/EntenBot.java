import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.Map;

import org.telegram.telegrambots.api.methods.send.SendMessage;
import org.telegram.telegrambots.api.objects.Message;
import org.telegram.telegrambots.api.objects.Update;
import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.exceptions.TelegramApiException;
import org.telegram.telegrambots.logging.BotLogger;

import java.sql.Timestamp;
import java.util.HashMap;

public class EntenBot extends TelegramLongPollingBot {
	/**
	 * A chat is blocked till a date and during this time
	 * the chat will not answer to any normal triggers
	 **/
	private class Block {
		private Timestamp till;

		/**
		 * Creates a block
		 **/
		public Block(int minutes) {
			this.till = new Timestamp(System.currentTimeMillis()+(60000*minutes));
		}

		public boolean isBlocked() {
			Timestamp current = new Timestamp(System.currentTimeMillis());
			if (current.before(this.till)) {
				return true;
			}
			return false;
		}
	}

	private String botUsername;
	private String botToken;
	private String git;
	private Map<Long, Block> blocks;
	private String dsgvo;

	public EntenBot(String username, String token, String git, String dsgvo) {
		this.botUsername = username;
		this.botToken = token;
		this.git = git;
		this.blocks = new HashMap<Long, Block>();
		this.dsgvo = dsgvo;
	}

	@Override
	public String getBotUsername() {
		return this.botUsername;
	}

	@Override
	public String getBotToken() {
		return this.botToken;
	}

	@Override
	public void onUpdateReceived(Update update) {
		if (update.hasMessage() && update.getMessage().hasText()) {
			String text = update.getMessage().getText().toLowerCase();
			SendMessage message = new SendMessage();
			long chat_id = update.getMessage().getChatId();
			message.setChatId(chat_id);
			Block block = this.blocks.get(chat_id);
			// trigger every time
			if(text.equals("/license") || text.equals("/license"+this.getBotUsername())) {
				String msg = "Welcome!\nThis bot is a program which is available under the MIT license at " + this.git;
				message.setText(msg);
			} else if(text.equals("/dsgvo") || text.equals("/dsgvo"+this.getBotUsername())) {
				String msg = this.dsgvo;
				message.setText(msg);
			}
			// don't trigger if is feeded
			else if(block == null || !block.isBlocked()) {
				if(block != null && !block.isBlocked()) {
					blocks.remove(block);
				}
				if(text.contains("ente")) {
					message.setText("*QUACK!*");
				}
				else if(text.matches(".*f+o+s+s+.*")) {
					message.setText("*FOOOOOOOSSSS <3!*");
				} else if(text.contains("turmbraeu") || text.contains("turmbräu") || text.contains("git") || text.contains("love")) {
					message.setText("*<3*");
				} else if(text.contains("svn") || text.contains("subversion")) {
					message.setText("*QUAAAAACKKKK😡!!*");
				} else if(text.contains("bread") || text.contains("brot")) {
					message.setText("Mmhhh");
					this.blocks.put(chat_id, new Block(10));
				} else {
					return;
				}
			} else {
				return;
			}
			message.setParseMode("markdown");
			try {
				sendMessage(message); // Call method to send the message
			} catch (TelegramApiException e) {
				e.printStackTrace();
			}
		}
	}

}
